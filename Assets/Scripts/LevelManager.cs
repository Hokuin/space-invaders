﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    #region Initialization
    public static LevelManager Instance;
    public GameObject startGameScreen;
    #endregion

    #region Levels
    [Header("Levels")]
    public LevelData[] levels;
    public LevelData currentLevelData;
    private int levelId;
    public LevelGenerator lvlGenerator;
    public StatsManager statsManager;
    #endregion

    #region Barriers
    [Header("Barriers")]
    public GameObject barrierPrefab;
    public Transform barriersHolder;
    [HideInInspector] public Queue<GameObject> barriersPool;
    #endregion

    #region Enemies
    [Header("Enemies")]
    public GameObject[] enemyPrefabs;
    public Transform enemiesHolder;
    [HideInInspector] public Queue<GameObject>[] enemiesPool;

    public float CurrentMovementSpeed { get; set; }
    public int EnemiesCount { get; set; }
    public int CurrentEnemiesShooting { get; set; }
    public int CurrentEnemiesShootingLimit { get; set; }
    #endregion

    #region Bullets
    [Header("Bullets")]
    public GameObject laserPrefab;
    public GameObject rocketPrefab;
    public Transform bulletsHolder;
    [HideInInspector] public Queue<GameObject> laserPool;
    [HideInInspector] public Queue<GameObject> rocketsPool;
    #endregion

    #region Other
    [Header("Other")]
    public GameObject heroPrefab;
    public Transform heroHolder;

    public GameObject specialPrefab;
    public Transform specialHolder;

    public GameObject explosionPrefab;
    public Transform explosionsHolder;
    [HideInInspector] public Queue<GameObject> explosionsPool;

    public GameOver gameover;
    #endregion

    private void Awake()
    {
        if (Instance != null && Instance != this) Destroy(gameObject);
        else Instance = this;

        barriersPool = new Queue<GameObject>();
        enemiesPool = new Queue<GameObject>[enemyPrefabs.Length];

        for (int i = 0; i < enemyPrefabs.Length; i++)
            enemiesPool[i] = new Queue<GameObject>();
        laserPool = new Queue<GameObject>();
        rocketsPool = new Queue<GameObject>();
        explosionsPool = new Queue<GameObject>();
    }

    private void Start()
    {
        ResetLevelManager();
    }

    public void StartGame()
    {
        Invoke("LoadNextLevel", 0.5f);
        InvokeRepeating("AllEnemiesDestoryed", 1f, 0.5f);
        startGameScreen.SetActive(false);
    }

    public void ResetLevelManager()
    {
        levelId = -1;
        CurrentMovementSpeed = currentLevelData.movementSpeed;
        CurrentEnemiesShooting = 0;
        CurrentEnemiesShootingLimit = 0;

        foreach (Enemy enemy in enemiesHolder.GetComponentsInChildren<Enemy>())
            PushToPool(enemiesPool[enemy.TypeId], enemy.gameObject);

        foreach (Bullet bullet in bulletsHolder.GetComponentsInChildren<Bullet>())
            switch (bullet.myOwner)
            {
                case Bullet.Owner.enemy:
                    PushToPool(rocketsPool, gameObject);
                    break;
                case Bullet.Owner.hero:
                    PushToPool(laserPool, gameObject);
                    break;
            }

        statsManager.ResetStats();
    }

    private void LoadNextLevel()
    {
        CurrentMovementSpeed = currentLevelData.movementSpeed;
        CurrentEnemiesShootingLimit = 0;

        StartCoroutine(IncreaseShootingLimit(currentLevelData.maxEnemiesShooting));

        levelId = Mathf.Min(levelId + 1, levels.Length - 1);
        currentLevelData = levels[levelId];

        lvlGenerator.GenerateLevel();
    }

    public void IncreaseMovementSpeed()
    {
        for (int i = 0; i <= Mathf.Log(EnemiesCount, 2); i++)
        {
            float powerOfTwo = Mathf.Pow(2, i);

            if (powerOfTwo == EnemiesCount)
            {
                CurrentMovementSpeed += currentLevelData.speedIncreaseValue;
                break;
            }
        }
    }

    public void SwapEnemiesMovementMode(Enemy.MovementDirection dir)
    {
        foreach (Enemy enemy in enemiesHolder.GetComponentsInChildren<Enemy>())
        {
            enemy.SwapMovementMode(dir);
        }
    }

    private void AllEnemiesDestoryed()
    {
        if (CurrentEnemiesShooting == 0)
        {
            bool reload = true;

            foreach (Enemy enemy in enemiesHolder.GetComponentsInChildren<Enemy>())
            {
                if (!reload || enemy.gameObject.activeSelf)
                {
                    reload = false;
                    break;
                }
            }

            if (reload) LoadNextLevel();
        }
    }

    private IEnumerator IncreaseShootingLimit(int maxAmount)
    {
        while (CurrentEnemiesShootingLimit < maxAmount)
        {
            yield return new WaitForSeconds(2f);
            CurrentEnemiesShootingLimit++;
        }
    }

    public void Stop()
    {
        CancelInvoke("AllEnemiesDestoryed");
        StopAllCoroutines();
    }

    public GameObject PullFromPool(Queue<GameObject> collection)
    {
        if (collection.Count > 0)
        {
            GameObject obj = collection.Dequeue();
            obj.SetActive(true);
            return obj;
        }
        return null;
    }

    public void PushToPool(Queue<GameObject> collection, GameObject obj)
    {
        obj.SetActive(false);
        collection.Enqueue(obj);
    }
}
