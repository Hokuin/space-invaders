﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public enum Owner { hero, enemy};
    public Owner myOwner;

    public float speed;

    private LevelManager manager;
    private LevelGenerator lvlGen;
    private StatsManager stats;

    private void Start()
    {
        manager = LevelManager.Instance;
        lvlGen = manager.lvlGenerator;
        stats = manager.statsManager;
    }

    private void Update()
    {
        CheckIfOutside();
    }

    private void FixedUpdate()
    {
        Vector3 direction = new Vector3();
        switch (myOwner)
        {
            case Owner.hero:
                direction = Vector3.up;
                break;
            case Owner.enemy:
                direction = Vector3.down;
                break;
        }

        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime);
    }

    private void CheckIfOutside()
    {
        bool proceed = false;

        switch (myOwner)
        {
            case Owner.hero:
                proceed = (transform.position.y > lvlGen.positionLimitsY[1] + 1f);
                break;
            case Owner.enemy:
                proceed = (transform.position.y < lvlGen.positionLimitsY[0]);
                break;
        }

        if (proceed)
            DisableBullet();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject obj = collision.gameObject;

        switch (myOwner)
        {
            case Owner.hero:
                
                if(obj.GetComponent<Bullet>() != null)
                {
                    Bullet bullet = obj.GetComponent<Bullet>();
                    manager.statsManager.TotalPoints += stats.pointsForBullet;
                    bullet.DisableBullet();
                    GenerateExplosion(obj);
                }
                else if (obj.GetComponent<Enemy>() != null)
                {
                    Enemy enemy = obj.GetComponent<Enemy>();
                    manager.statsManager.TotalPoints += stats.pointsForEnemies[enemy.TypeId];
                    manager.PushToPool(manager.enemiesPool[enemy.TypeId], obj);
                    if (!enemy.Decremented)
                    {
                        enemy.Decremented = true;
                        manager.EnemiesCount--;
                        manager.IncreaseMovementSpeed();
                    }
                    GenerateExplosion(obj);
                }
                else if (obj.GetComponent<Special>() != null)
                {
                    manager.statsManager.TotalPoints += stats.pointsForSpecial;
                    GenerateExplosion(obj);
                    Destroy(obj);
                }
                break;

            case Owner.enemy:

                if (obj.GetComponent<HeroController>() != null)
                {
                    manager.statsManager.DecreaseLife();
                    GenerateExplosion(obj);
                }
                
                break;
        }
        if (obj.GetComponent<Barrier>() != null)
        {
            manager.PushToPool(manager.barriersPool, obj);
            GenerateExplosion(obj);
        }
    }

    private void GenerateExplosion(GameObject explodedObject)
    {
        GameObject explosion = manager.PullFromPool(manager.explosionsPool);
        explosion.transform.position = explodedObject.transform.position;
        explosion.GetComponent<Explosion>().DisableExplosion();
        DisableBullet();
    }

    public void DisableBullet()
    {
        switch (myOwner)
        {
            case Owner.enemy:
                manager.CurrentEnemiesShooting--;
                manager.PushToPool(manager.rocketsPool, gameObject);
                break;
            case Owner.hero:
                manager.PushToPool(manager.laserPool, gameObject);
                break;
        }
    }
}
