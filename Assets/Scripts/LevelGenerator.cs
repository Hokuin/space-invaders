﻿using System.Collections;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    private LevelManager manager;
    private GameObject special;

    public GameObject heroShip;
    [HideInInspector] public Vector2 positionLimitsX;
    [HideInInspector] public Vector2 positionLimitsY;

    private void Awake()
    {
        Camera myCam = Camera.main;
        Vector3 p1 = myCam.ViewportToWorldPoint(new Vector3(0, 0, myCam.nearClipPlane));
        Vector3 p2 = myCam.ViewportToWorldPoint(new Vector3(1, 0, myCam.nearClipPlane));
        Vector3 p3 = myCam.ViewportToWorldPoint(new Vector3(1, 1, myCam.nearClipPlane));

        float width = (p2 - p1).magnitude;
        float height = (p3 - p2).magnitude;
        float borderX = Mathf.Min(0.1f * width, 2f);
        float borderY = Mathf.Min(0.1f * height, 0.5f);

        positionLimitsX = new Vector2(-width / 2 + borderX, width / 2 - borderX);
        positionLimitsY = new Vector2(-height / 2 + borderY, height / 2 - borderY);
    }

    private void Start()
    {
        manager = LevelManager.Instance;

        GenerateBarriersSet();
        GenerateEnemiesSet();
        GenerateLaserSet();
        GenerateRocketsSet();
        GenerateExplosionsSet();
    }

    public void GenerateLevel()
    {
        GenerateHero();
        GenerateBarriers();
        GenerateEnemies();
    }

    private void GenerateHero()
    {
        if (heroShip == null)
        {
            Vector2 heroPosition = new Vector2(0, positionLimitsY[0]);

            heroShip = Instantiate
                (original: manager.heroPrefab,
                position: heroPosition,
                rotation: manager.heroPrefab.transform.rotation,
                parent: manager.heroHolder);
        }
    }

    private void GenerateBarriers()
    {
        LevelData data = manager.currentLevelData;

        float width = positionLimitsX[1] - positionLimitsX[0];
        float step = width / 5;

        Vector2 position = new Vector2(positionLimitsX[0] + step, positionLimitsY[0] + 1.5f);

        for (int i = 0; i < 4; i++)
        {
            GameObject obj = manager.PullFromPool(manager.barriersPool);
            obj.transform.position = position;
            position.x += step;
        }
    }

    public void GenerateEnemies()
    {
        StopAllCoroutines();

        LevelData data = manager.currentLevelData;

        int fixedEnemiesInRow = Mathf.RoundToInt((positionLimitsX[1] - positionLimitsX[0]) / data.stepBetween);

        if (data.enemiesInRow > fixedEnemiesInRow)
            data.enemiesInRow = fixedEnemiesInRow;

        Vector2 enemyPosition = new Vector2(0 - data.stepBetween * data.enemiesInRow / 2, positionLimitsY[1]);

        for (int i = 0; i < data.numberOfRows; i++)
        {
            int id = Mathf.Min(i, manager.enemyPrefabs.Length - 1);

            for (int j = 0; j < data.enemiesInRow; j++)
            {
                enemyPosition.x += data.stepBetween;
                GenerateEnemy(id, enemyPosition);
            }
            enemyPosition.x = 0 - data.stepBetween * data.enemiesInRow / 2;
            enemyPosition.y -= 1f;
        }

        manager.EnemiesCount = data.numberOfRows * data.enemiesInRow;

        StartCoroutine(SpawnSpecial());
    }
    public void GenerateEnemy(int typeId, Vector3 pos)
    {
        GameObject enemy = manager.PullFromPool(manager.enemiesPool[typeId]);
        enemy.GetComponent<Enemy>().ResetMe();
        enemy.transform.position = pos;
    }

    public void GenerateBarriersSet()
    {
        for (int i = 0; i < 20; i++)
        {
            GameObject obj = Instantiate
                (original: manager.barrierPrefab,
                parent: manager.barriersHolder
                );
            manager.PushToPool(manager.barriersPool, obj);
        }
    }

    public void GenerateEnemiesSet()
    {
        LevelData data = manager.currentLevelData;

        for (int i = 0; i < 20; i++)
        {
            int id = Mathf.Min(i, manager.enemyPrefabs.Length - 1);

            GenerateEnemiesSetOfType(id, 30);
        }
    }
    public void GenerateEnemiesSetOfType(int typeId, int count)
    {
        GameObject obj = Instantiate
            (original: manager.enemyPrefabs[typeId],
            parent: manager.enemiesHolder
            );
        
        obj.GetComponent<Enemy>().Decremented = false;

        manager.PushToPool(manager.enemiesPool[typeId], obj);

        for (int i = 0; i < count-1; i++)
        {
            manager.PushToPool
                (manager.enemiesPool[typeId],
                Instantiate(original: obj, parent: manager.enemiesHolder));
        }
    }

    public void GenerateLaserSet()
    {
        for (int i = 0; i < 100; i++)
        {
            GameObject obj = Instantiate
                (original: manager.laserPrefab,
                parent: manager.bulletsHolder
                );
            manager.PushToPool(manager.laserPool, obj);
        }
    }
    public void GenerateRocketsSet()
    {
        for (int i = 0; i < 50; i++)
        {
            GameObject obj = Instantiate
                (original: manager.rocketPrefab,
                parent: manager.bulletsHolder
                );
            manager.PushToPool(manager.rocketsPool, obj);
        }
    }
    public void GenerateExplosionsSet()
    {
        for (int i = 0; i < manager.currentLevelData.enemiesInRow * manager.currentLevelData.numberOfRows; i++)
        {
            GameObject obj = Instantiate
                (original: manager.explosionPrefab,
                parent: manager.explosionsHolder
                );
            manager.PushToPool(manager.explosionsPool, obj);
        }
    }

    private IEnumerator SpawnSpecial()
    {
        if (special != null) Destroy(special);
        Vector2 spawnPosition = new Vector2(positionLimitsX[1] + 2f, positionLimitsY[1]);
        Vector2 target = new Vector2(positionLimitsX[0] - 3f, positionLimitsY[1]);

        while (true)
        {
            yield return new WaitForSeconds(10f);
            special = Instantiate(manager.specialPrefab, spawnPosition, manager.specialPrefab.transform.rotation, manager.specialHolder);

            while (special != null)
            {
                if (special.transform.position.x < positionLimitsX[0] - 2f)
                    Destroy(special);
                else 
                    special.transform.position = Vector2.MoveTowards(special.transform.position, target, 2f * Time.deltaTime);
                yield return null;
            }
        }
    } 
}
