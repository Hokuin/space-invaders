﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Attack))]
public class HeroController : MonoBehaviour
{
    public float heroSpeed;

    public SpriteRenderer[] engineFlames;
    
    private bool[] movement = new bool[] { false, false };

    private LevelManager manager;
    private LevelGenerator lvlGenerator;
    private Attack attack;

    private void Awake()
    {
        attack = GetComponent<Attack>();
        SetEnginesEnabled(false);   
    }

    private void Start()
    {
        manager = LevelManager.Instance;
        lvlGenerator = manager.lvlGenerator;
    }

    private void Update()
    {
        movement[0] = Input.GetKey(KeyCode.LeftArrow);
        movement[1] = Input.GetKey(KeyCode.RightArrow);

        if (movement[0] || movement[1]) SetEnginesEnabled(true);
        else SetEnginesEnabled(false);
        
        if (movement[0]) Move(-1);
        if (movement[1]) Move(1);
        
        if (Input.GetKeyDown(KeyCode.Space)) attack.HeroAttack();
    }

    private void Move(int direction)
    {
        Vector3 newPosition = transform.position;
        newPosition.x += direction * heroSpeed * Time.deltaTime;

        if (newPosition.x < lvlGenerator.positionLimitsX[0])
            newPosition.x = lvlGenerator.positionLimitsX[0];
        else if (newPosition.x > lvlGenerator.positionLimitsX[1])
            newPosition.x = lvlGenerator.positionLimitsX[1];

        if (newPosition != transform.position)
            transform.position = Vector3.MoveTowards(transform.position, newPosition, Mathf.Infinity);
    }

    private void SetEnginesEnabled(bool _enabled)
    {
        foreach (SpriteRenderer engine in engineFlames) engine.enabled = _enabled;
        StartCoroutine(EngineBlitz());
    }

    private IEnumerator EngineBlitz()
    {
        while (true)
        {
            foreach (SpriteRenderer fx in engineFlames) fx.flipX = !fx.flipX;
            yield return null;
        }
    }

    private void OnDestroy()
    {
        manager.gameover.ShowGameOverScreen();
    }
}
