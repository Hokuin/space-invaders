﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Attack))]
public class Enemy : MonoBehaviour
{
    public enum MovementDirection { right, left, down };
    public MovementDirection direction;

    public int TypeId;
    public bool Decremented { get; set; }

    private LevelManager manager;
    private LevelGenerator lvlGenerator;

    private void Start()
    {
        manager = LevelManager.Instance;
        lvlGenerator = manager.lvlGenerator;
    }

    private void Update()
    {
        Move();
        if (transform.position.y < manager.lvlGenerator.positionLimitsY[0])
            Destroy(manager.lvlGenerator.heroShip);
    }

    private void Move()
    {
        switch (direction)
        {
            case MovementDirection.left:
                transform.position = Vector3.MoveTowards
                    (transform.position,
                    transform.position + 100 * Vector3.left,
                    manager.CurrentMovementSpeed * Time.deltaTime);

                if (transform.position.x < lvlGenerator.positionLimitsX[0])
                    manager.SwapEnemiesMovementMode(MovementDirection.right);
                break;

            case MovementDirection.right:
                transform.position = Vector3.MoveTowards
                    (transform.position,
                    transform.position + 100 * Vector3.right,
                    manager.CurrentMovementSpeed * Time.deltaTime);

                if (transform.position.x > lvlGenerator.positionLimitsX[1])
                    manager.SwapEnemiesMovementMode(MovementDirection.left);
                break;
            default:
                break;
        }
    }

    public void SwapMovementMode(MovementDirection dir)
    {
        direction = MovementDirection.down;
        StartCoroutine(MoveDown(dir));
    }

   public IEnumerator MoveDown(MovementDirection nextMode)
   {
        Vector3 target = transform.position;
        target.y -= 1f;

        while (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, manager.CurrentMovementSpeed * Time.deltaTime);
            yield return null;
        }
        direction = nextMode;
   }

    public void ResetMe()
    {
        direction = MovementDirection.right;
    }
}
