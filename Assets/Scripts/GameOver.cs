﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public GameObject gameoverPanel;
    public Text currentScoreValue;
    public Text currentBestScoreValue;
    public Text newRecord;

    public bool IsTriggered { get; set; }

    private void Awake()
    {
        IsTriggered = false;
        gameoverPanel.SetActive(false);
        newRecord.enabled = false;
    }

    public void ShowGameOverScreen()
    {
        if (IsTriggered == false)
        {
            IsTriggered = true;

            LevelManager.Instance.Stop();

            int currentScore = LevelManager.Instance.statsManager.TotalPoints;
            int bestScore = PlayerPrefs.GetInt("BestScore");

            if (currentScore > bestScore)
            {
                PlayerPrefs.SetInt("BestScore", currentScore);
                bestScore = currentScore;
                StartCoroutine(NewRecordSignal());
            }

            currentScoreValue.text = currentScore.ToString();
            currentBestScoreValue.text = bestScore.ToString();

            newRecord.enabled = false;
            gameoverPanel.SetActive(true);
        }
    }

    private IEnumerator NewRecordSignal()
    {
        while (true)
        {
            newRecord.enabled = !newRecord.enabled;
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void RetryButton()
    {
        IsTriggered = false;
        newRecord.enabled = false;
        gameoverPanel.SetActive(false);

        LevelManager.Instance.ResetLevelManager();
        LevelManager.Instance.StartGame();
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
