﻿using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour
{
    public Image[] healthIconsLeft;
    public int LivesLeft { get; set; }

    public int TotalPoints { get; set; }

    public int[] pointsForEnemies = new int[4] { 50, 30, 20, 10 };
    public int pointsForBullet = 15;
    public int pointsForSpecial = 200;

    public Text score;

    private void Start()
    {
        ResetStats();
    }

    private void Update()
    {
        score.text = TotalPoints.ToString() + "  ";
    }

    public void ResetStats()
    {
        TotalPoints = 0;
        LivesLeft = healthIconsLeft.Length;
        for (int i = 0; i < LivesLeft; i++)
        {
            healthIconsLeft[i].enabled = true;
        }
    }

    public void DecreaseLife()
    {
        LivesLeft--;
        if (LivesLeft >= 0)
            healthIconsLeft[LivesLeft].enabled = false;
        else
            Destroy(LevelManager.Instance.lvlGenerator.heroShip);
        
    }
}
